# coding: utf-8
import collections
import re
from bs4 import BeautifulSoup
from codecs import getwriter
import json
import requests
from requests.adapters import HTTPAdapter
from signal import signal, SIGINT, SIGTERM
from sys import stdout, exit, exc_info, stderr

TIMEOUT = 90
MAX_RETRIES = 20
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}


def clean(val):
    return val.strip()


class Client(object):
    FIRST_LETTER = {u'–', u'—', u'-', '('}

    def __init__(self, BASE):
        self._BASE = BASE
        self._session = requests.Session()
        self._session.headers.update(headers)
        self._session.mount(self._BASE['url'], HTTPAdapter(max_retries=MAX_RETRIES))
        self._result = {}
        self.GOOD_TAGS = ['p']
        for i in range(1, 7):
            self.GOOD_TAGS.append('h%i' % i)

    def _add_word(self, html, title):
        if html.find('b'):
            word = clean(html.find('b').text)
            description = clean(html.text)
            if not word or not description:
                return
            if word[-1] in self.FIRST_LETTER:
                word = word[:-1]
        else:
            word = ''
            description = clean(html.text)
            for x in description:
                if x in self.FIRST_LETTER or not x == x.upper():
                    break
                word += x
            word = clean(word)
            
        if not word or not description:
            return
        description = clean(description[len(word):])
        if not description:
            return
        if description[0] == '(':
            description = clean(description[description.find(')')+1:])
        if description[0] in self.FIRST_LETTER:
            description = clean(description[1:])
        if not description:
            return

        if word not in self._result:
            self._result[word] = {
                'url': self._BASE['url'],
                'title': title,
                'desc': {
                    'name': word,
                    'description': []
                }
            }
        self._result[word]['desc']['description'].append(description)

    def process(self):
        response = self._session.get(self._BASE['url'])
        soup = BeautifulSoup(response.content, 'html.parser')
        title = clean(soup.find('head').find('title').text)
        body = soup.find('body').find('div', attrs={'class': 'articlebody'})
        for tag in self.GOOD_TAGS:
            for html in body.findAll(tag):
                self._add_word(html, title)

        for word in self._result:
            if len(self._result[word]['desc']['description']) == 1:
                self._result[word]['desc']['description'] = self._result[word]['desc']['description'][0]

    @property
    def result(self):
        return collections.OrderedDict(sorted(self._result.items(), key=lambda x: x[0])).values()


def main():
    sout = getwriter("utf8")(stdout) 
    serr = getwriter("utf8")(stderr)
    URLS = {
        'politics': {
            'url': 'http://pandia.ru/text/77/475/3457.php',
        }
    }
    client = Client(URLS['politics'])
    try:
        client.process()
        for res in client.result:
            sout.write(json.dumps(res, ensure_ascii=False) + "\n")
    except Exception as e:
        exc_traceback = exc_info()[2]
        filename = line = None
        while exc_traceback is not None:
            f = exc_traceback.tb_frame
            line = exc_traceback.tb_lineno
            filename = f.f_code.co_filename
            exc_traceback = exc_traceback.tb_next
        serr.write(json.dumps({
            'error': True,
            'details': {
                'message': str(e),
                'file': filename,
                'line': line
            }
        }, ensure_ascii=False) + "\n")


if __name__ == "__main__": 
    def signal_handler(signal, frame): 
        exit(0)
    signal(SIGINT, signal_handler)
    signal(SIGTERM, signal_handler)
    main() 
